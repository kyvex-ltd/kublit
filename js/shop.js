const items = {

    "bomb_defuser": {
        "name": "Bomb Defuser",
        "description": "Defuses a bomb",
        "price": 100,
        "image": "bomb-defuser.png",
    },

    "cash_bonus": {
        "name": "Cash Bonus",
        "description": "Grants you a 2x cash bonus",
        "price": 45,
        "image": "cash-bonus.png",
    },

    "health": {
        "name": "Health",
        "description": "Grants 1 extra health point. Stackable up to 5 times.",
        "price": 25,
        "image": "health.png",
    },

    "mining_power": {
        "name": "Mining Power",
        "description": "Make mining Tough Kublits easier.",
        "price": 50,
        "image": "mining-power.png"
    }
}

const shop = document.querySelector('.shop__container');

for (const item in items) {

    const itemElement = document.createElement('div');
    itemElement.classList.add('item');

    const itemImage = document.createElement('img');
    itemImage.src = `./img/items/${items[item].image}`;
    itemImage.alt = items[item].name;
    itemImage.classList.add('item__image');
    itemImage.width = 100;
    itemImage.height = 100;

    const itemName = document.createElement('h3');
    itemName.innerText = items[item].name;
    itemName.classList.add('item__name');

    const itemDescription = document.createElement('p');
    itemDescription.innerText = items[item].description;
    itemDescription.classList.add('item__description');

    const itemPrice = document.createElement('p');
    itemPrice.innerText = `Price: ${items[item].price}`;
    itemPrice.classList.add('item__price');

    const itemButton = document.createElement('button');
    itemButton.innerText = 'Buy';
    itemButton.classList.add('item__button');

    itemElement.appendChild(itemImage);
    itemElement.appendChild(itemName);
    itemElement.appendChild(itemDescription);
    itemElement.appendChild(itemPrice);
    itemElement.appendChild(itemButton);

    shop.appendChild(itemElement);

}
