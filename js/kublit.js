class Kublit {
    constructor(health, type = 'Normal', row, col, board, layerIndex) {
        this.health = health;

        type = type.toLowerCase();
        if (type === 'normal' || type === 'bomb') this.health = health;
        else if (type === 'bedrock') this.health = 0;
        else if (type === 'tough') this.health = Math.floor(Math.random() * 100) + 1;
        else if (type === 'transparent') this.health = 0;
        else {
            console.error(`Invalid Kublit type: ${type}`);
            this.health = 0;
            this.type = 'Normal';
        }

        this.type = type.charAt(0).toUpperCase() + type.slice(1);
        this.element = this.createKublitElement();
        this.row = row;
        this.col = col;
        this.board = board;
        this.layerIndex = layerIndex;

        this.element.addEventListener('click', this.onClick.bind(this));
        this.element.addEventListener('touchstart', this.onClick.bind(this));
        this.updateDisplay();
    }

    createKublitElement() {
        const kublitElement = document.createElement('div');
        kublitElement.classList.add('kublit', this.type.toLowerCase());
        return kublitElement;
    }

    async onClick() {
        this.health -= 10;
        this.updateDisplay();
        await this.board.handleKublitClick(this);
    }

    updateDisplay() {
        this.element.innerText = `${this.health > 0 ? this.health : '0'}`;
    }
}

export default Kublit;