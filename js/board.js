import Kublit from './kublit.js';
import { notify, updateX, score, level } from './main.js';

class Board {

    constructor(config, container = document.querySelector('.kublit__container')) {
        this.rows = config.rows;
        this.cols = config.cols;
        this.layers = config.layers || [];
        this.currentLayerIndex = 0;
        this.highestLayer = 0;
        this.kublits = [];
        this.element = container ||  document.querySelector('.kublit__container');
        this.element.style.gridTemplateRows = `repeat(${this.rows}, 1fr)`;
        this.element.style.gridTemplateColumns = `repeat(${this.cols}, 1fr)`;
        this.createKublitsForLayers();
    }

    createKublitsForLayers() {

        this.layers[0].kublits.forEach((kublitConfig, index) => {
            const {health, type} = kublitConfig;
            const row = Math.floor(index / this.cols);
            const col = index % this.cols;
            const kublit = new Kublit(health, type, row, col, this, 0);
            this.kublits.push([kublit]);
            this.element.appendChild(kublit.element);
        });
    }

    hasClickableKublits() {
        const kublitElements = this.element.getElementsByClassName('kublit');

        for (const kublitElement of kublitElements) {
            if (!kublitElement.classList.contains('bedrock') && !kublitElement.classList.contains('transparent')) {
                const kublitType = kublitElement.classList.contains('bomb') ? 'bomb' :
                    kublitElement.classList.contains('tough') ? 'tough' :
                        'normal';

                // Check if it's clickable based on its type
                if (kublitType !== 'bedrock' && kublitType !== 'transparent') return true;
            }
        }
        return false;
    }

    async handleKublitClick(clickedKublit) {

        // If element has class "bedrock" or "damaged", do nothing
        if (clickedKublit.element.classList.contains('bedrock') ||
            clickedKublit.element.classList.contains('damaged') ||
            clickedKublit.element.classList.contains('transparent')) {
            return;
        }

        const audio = new Audio('audio/tap.wav');
        await audio.play();

        if (clickedKublit.health <= 0) {
            const nextLayerIndex = clickedKublit.layerIndex + 1;
            updateX('score', 20);

            // Check if the next layer is the highest layer
            if (nextLayerIndex > this.highestLayer) {
                this.highestLayer = nextLayerIndex;
                updateX('score', 100);
                notify(`New layer unlocked! Layer ${this.highestLayer + 1}`);
            }

            if (nextLayerIndex < this.layers.length) {
                const nextLayerKublits = this.layers[nextLayerIndex].kublits;
                const nextKublitIndex = clickedKublit.row * this.cols + clickedKublit.col;

                // Check if the nextKublitIndex is within the valid range of the next layer's Kublits array
                if (nextKublitIndex < nextLayerKublits.length) {
                    const {health, type} = nextLayerKublits[nextKublitIndex];

                    // Replace the Kublit with a Kublit from the next layer at the same position
                    const newKublit = new Kublit(health, type, clickedKublit.row, clickedKublit.col, this, nextLayerIndex);
                    this.kublits[clickedKublit.row * this.cols + clickedKublit.col].push(newKublit);

                    // Position the new Kublit at the same row and column
                    newKublit.element.style.gridRow = clickedKublit.row + 1;
                    newKublit.element.style.gridColumn = clickedKublit.col + 1;

                    // Add the explosion class
                    clickedKublit.element.classList.add('damaged');

                    // wait 500ms
                    setTimeout(() => {
                        this.element.appendChild(newKublit.element);
                        clickedKublit.element.remove();
                    }, 500);

                } else {
                    // If there are no more layers, create a bedrock Kublit
                    const bedrockKublit = this.createBedrockKublit(clickedKublit.row, clickedKublit.col);
                    this.kublits[clickedKublit.row * this.cols + clickedKublit.col].push(bedrockKublit);
                    clickedKublit.element.classList.add('damaged');

                    setTimeout(() => {
                        this.element.appendChild(bedrockKublit.element);
                        clickedKublit.element.remove();
                    }, 500);
                }
            } else {
                const bedrockKublit = this.createBedrockKublit(clickedKublit.row, clickedKublit.col);
                this.kublits[clickedKublit.row * this.cols + clickedKublit.col].push(bedrockKublit);
                this.element.appendChild(bedrockKublit.element);
                clickedKublit.element.remove();
            }
        }

        // Check if there are any clickable Kublits left
        if (!this.hasClickableKublits(this)) {
            const randomStr = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            notify(`You got the code. ${randomStr}`);
        }

    }


    createBedrockKublit(row, col) {
        const bedrockKublit = new Kublit(0, 'Bedrock', row, col, this, -1); // -1 indicates bedrock

        // Add the "bedrock" class to the bedrock Kublit element
        bedrockKublit.element.classList.add('bedrock');

        // Position the bedrock Kublit at the same row and column
        bedrockKublit.element.style.gridRow = row + 1; // Adjust for CSS grid 1-indexing
        bedrockKublit.element.style.gridColumn = col + 1; // Adjust for CSS grid 1-indexing

        return bedrockKublit;
    }
}

export default Board;