import Board from './board.js';

let score = 0;
let time = 0;
let level = 1;

let scoreElement;
let timeElement;
let levelElement;

try {
    scoreElement = document.querySelector('.score').firstElementChild;
    timeElement = document.querySelector('.time').firstElementChild;
    levelElement = document.querySelector('.level').firstElementChild;
} catch (error) {

}

function notify(message) {
    const notification = document.createElement('div');
    notification.classList.add('notification');
    notification.innerText = message;
    document.body.appendChild(notification);
    setTimeout(() => {
        notification.classList.add('fade-out');
        setTimeout(() => {
            notification.remove();
        }, 500);
    }, 3000);
}

function updateX(type = 'score', value = 0) {
    if (type === 'score') score += value;
    else if (type === 'time') time += value;
    else if (type === 'level') level += value;
    else console.error(`Invalid type: ${type}`);
}

// Load the JSON configuration
async function loadBoardConfig(level = 1) {

    notify(`Loading level ${level}...`);

    try {

        let response;
        let startTime = Date.now();

        // If the level begins with rem: then it's a level from the GitLab repository
        if (level.toString().startsWith('gl:')) {
            const levelName = level.toString().split(':')[1];
            console.log(`Loading level ${levelName} from GitLab`);
            const gitlabUrl = `https://gitlab.com/api/v4/projects/50847451/repository/files/levels%2F${levelName}.json/raw?ref=main`;
            response = await fetch(`${gitlabUrl}`);
        } else {
            console.log(`Loading level ${level} from local storage`);
            response = await fetch(`./levels/${level}.json`);
        }

        if (!response.ok) {
            notify(`Error loading level ${level} - ${response.status} ${response.statusText}`);
            throw new Error(`${response.status} ${response.statusText}`);
        }

        const config = await response.json();

        notify(`Level ${config.data.name} loaded!`);
        console.log(`Loaded level ${config.data.name} (${level})`);
        console.log(`   > Took | ${Date.now() - startTime}ms`);
        console.log(`   > By   | ${config.data.author}`);
        console.log(`   > Name | ${config.data.name}`);
        console.log(`   > Msg  | ${config.data.message}`);
        console.log(`   > Desc | ${config.data.description}`);
        console.log(`====================================`)

        notify(config.data.message ? config.data.message : config.data.name);
        return new Board(config);
    } catch (error) {
        console.error('Error loading board configuration:', error);
    }
}

async function main() {
// Monitor time and update UI elements
    setInterval(() => {
        time += 1;

        timeElement.innerText = `Time: ${Math.floor(time / 10 / 60)}:${Math.floor(time / 10 % 60).toString().padStart(2, '0')}`;
        scoreElement.innerText = `Score: ${score}`;
        levelElement.innerText = `Level ${level}`;

    }, 100);

    // Get the current level from the URL
    const urlParams = new URLSearchParams(window.location.search);
    const levelParam = urlParams.get('level');

    // Load the board configuration and start the game
    await loadBoardConfig(levelParam ? levelParam : 1).then(board => {
        console.log('Board loaded!');
    });
}

// If page is play.html
if (window.location.pathname.endsWith('play.html')) main().then(r => console.log('Game started!'));


export {updateX, score, time, level, notify};