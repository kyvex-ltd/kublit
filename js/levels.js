
const gitlabUrl = "https://gitlab.com/api/v4/projects/50847451/repository/files";
let levels = []

/**
 * @name fetchLevels
 * @desc Fetches levels from the dataset.json in the GitLab repository
 * @returns {Promise} A promise that resolves to an array of level names and paths
 */

async function fetchLevels() {
    try {
        const response = await fetch(`${gitlabUrl}/dataset.json/raw?ref=main`);

        return await response.json();
    } catch (error) {
        console.error('Error fetching levels:', error);
    }
}

/**
 * @name fetchLevelData
 * @desc Fetches a level from the GitLab repository
 * @param {string} file The name of the level file
 * @returns {Promise} A promise that resolves to the level data
 */

async function fetchLevelData(file) {
    try {
        const response = await fetch(`${gitlabUrl}/levels%2F${file}/raw?ref=main`);
        return await response.json();
    } catch (error) {
        console.error('Error fetching level:', error);
    }
}

/**
 * @name loadLevelPage
 * @desc Loads a page of levels
 * @param {number} page The page number
 * @param {number} pageSize The number of levels per page
 * @returns {Promise} A promise that resolves to an array of level data
 */

async function loadLevelPage(page = 1, pageSize = 10) {
    try {
        const response = await fetch(`${gitlabUrl}/dataset.json/raw?ref=main`);
        const data = await response.json();
        const start = (page - 1) * pageSize;
        const end = start + pageSize;
        return data.slice(start, end);
    } catch (error) {
        console.error('Error loading level page:', error);
    }
}

/**
 * @name addLevelToContainer
 * @desc Adds a level to the level container
 * @param {object} level The level data
 * @param {number} index The index of the level
 * @param {HTMLElement} container The container to add the level to
 * @param {string} shortName The short name of the level
 * @returns {void}
 */

function addLevelToContainer(level, index, container, shortName) {


    const levelElement = document.createElement('div');
    levelElement.classList.add('level');
    levelElement.dataset.level = `gl:${shortName}`;

    const levelPreview = document.createElement('img');
    levelPreview.classList.add('level__preview');
    levelPreview.src = `${gitlabUrl}/img%2F${shortName}.png/raw?ref=main`;

    const levelInfo = document.createElement('div');
    levelInfo.classList.add('level__info');

    const levelTitle = document.createElement('h3');
    levelTitle.innerText = level.data.name;

    const levelAuthor = document.createElement('p');
    levelAuthor.innerHTML = `By ${level.data.author}`;

    const levelDifficulty = document.createElement('p');
    levelDifficulty.innerHTML = `Difficulty: <span class="level__difficulty">${level.data.difficulty}</span>`;

    const levelPlayButton = document.createElement('button');
    levelPlayButton.classList.add('level__play-button');
    levelPlayButton.innerText = 'Play';

    levelPlayButton.addEventListener('click', () => {
        window.location.href = `play.html?level=gl:${shortName}`;
    });

    levelInfo.appendChild(levelTitle);
    levelInfo.appendChild(levelAuthor);
    levelInfo.appendChild(levelDifficulty);
    levelInfo.appendChild(levelPlayButton);
    levelElement.appendChild(levelPreview);
    levelElement.appendChild(levelInfo);
    container.appendChild(levelElement);


}

async function main() {
    levels = await fetchLevels();

    for (let i = 0; i < levels.length; i++) {
        const level = levels[i];
        console.log(level);
        const levelData = await fetchLevelData(level.file);
        addLevelToContainer(levelData, i, document.querySelector('.levels__container'), level.file.replace('.json', ''));
    }
}

main();
